/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

package de.fraunhofer.toolboxexample;

import java.net.URI;
import java.net.URL;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;

import de.fraunhofer.iais.eis.ids.jsonld.Serializer;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.idsintegrationtoolbox.DscOperator;
import org.siliconeconomy.idsintegrationtoolbox.model.dsc.PaymentMethod;
import org.siliconeconomy.idsintegrationtoolbox.model.input.ArtifactInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.CatalogInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.ContractInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.OfferedResourceInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.RepresentationInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.RuleInput;
import org.siliconeconomy.idsintegrationtoolbox.utils.PolicyUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * The Provider offers endpoints to create and register an example resource and to get an example
 * passenger information.
 *
 * @author Haydar Qarawlus
 */
@Log4j2
@RestController
@RequiredArgsConstructor
public class Provider {

    /** The serializer used to serialize objects. */
    private final @NonNull Serializer serializer;

    /** the dsc operator. */
    private final @NonNull DscOperator dsc;

    /** The catalog title. */
    @Value("${catalog.title:Flight 0001 Baggage}")
    private String catalogTitle;
    /** The catalog description. */
    @Value("${catalog.description:The resource catalog containing baggage information}")
    private String catalogDescription;

    /** The data representation title. */
    @Value("${data-representation.title:DataRepresentationPas1}")
    private String dataRepresentationTitle;

    /** The access url. */
    @Value("${access-url:http://localhost:8081/passenger-details}")
    private String accessUrl;

    /** The artifact title. */
    @Value("${artifact.title:Passenger 1 Data}")
    private String artifactTitle;

    /** The contract title. */
    @Value("${contract.title:contract title}")
    private String contractTitle;

    /** The contract start time as ZonedDateTime string. */
    @Value("${contract.start-time:2021-08-06T13:33:44.995+02:00}")
    private String contractStartTime;

    /** The contract end time as ZonedDateTime string. */
    @Value("${contract.end-time:2021-09-06T13:33:44.995+02:00}")
    private String contractEndTime;

    /** The rule title. */
    @Value("${rule.title:Some Rule}")
    private String ruleTitle;

    /** The offered resource keywords, separated with a comma. */
    @Value("${offered-resource.keywords:Flight0001,Passenger}")
    private String offeredResourceKeywords;

    /** The offered resource publisher uri. */
    @Value("${offered-resource.publisher-uri:https://example.org/airline1}")
    private String offeredResourcePublisherUri;

    /** The offered resource license uri. */
    @Value("${offered-resource.license-uri:https://example.org/usageLicense}")
    private String offeredResourceLicenseUri;

    /** The offered resource owner uri. */
    @Value("${offered-resource.owner-uri:https://example.org/passengerDataOwner}")
    private String offeredResourceOwnerUri;

    /** The offered resource data access endpoint documentation uri. */
    @Value("${offered-resource.endpoints-docs-uri:https://example"
            + ".org/passengerDataAccessDocumentation}")
    private String offeredResourceEndpointsDocsUri;

    /** The offered resource title. */
    @Value("${offered-resource.title:PassengerBaggage1}")
    private String offeredResourceTitle;

    /** The offered resource description. */
    @Value("${offered-resource.description:Resource holding information about passenger baggage "
            + "of passenger 1}")
    private String offeredResourceDescription;

    /**
     * Route for creating and registering an example resource in the DSC.
     *
     * @return http code 200 if registering the resource was successful.
     */
    @GetMapping("create-resource")
    public ResponseEntity<String> createResource() {
        try {
            log.debug("Received a request to create a resource at the DSC!");

            // Create Resource Catalog
            final var catalogInput = new CatalogInput();
            catalogInput.setTitle(catalogTitle);
            catalogInput.setDescription(catalogDescription);

            // Create offered resource input
            final var offeredResourceInput = createOfferedResourceInput();

            // Create representation input
            final var dataRepresentation = new RepresentationInput();
            dataRepresentation.setMediaType("application/json");
            dataRepresentation.setTitle(dataRepresentationTitle);
            dataRepresentation.setLanguage("en");

            // Create artifact inputs
            final var accessUrlObject = new URL(this.accessUrl);

            // Create Artifact input
            final var artifactInput = new ArtifactInput(artifactTitle, accessUrlObject, null,
                    null, false);

            // Create contract input
            final var contractInput = new ContractInput(contractTitle,
                    ZonedDateTime.parse(contractStartTime),
                    ZonedDateTime.parse(contractEndTime));

            // Create rule input
            final var ruleInput = new RuleInput();
            ruleInput.setTitle(ruleTitle);
            ruleInput.setValue(serializer.serialize(PolicyUtils.buildProvideAccessRule(ruleTitle)));

            // Create resource
            dsc.workflows().resourceCreation().createResource(offeredResourceInput, catalogInput,
                    dataRepresentation, artifactInput, contractInput, ruleInput);

            return new ResponseEntity<>("Resource Created!", HttpStatus.OK);
        } catch (Exception e) {
            log.error("Exception caught: {}", e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Creates an example offered resource input.
     *
     * @return an example offered resource input.
     */
    private OfferedResourceInput createOfferedResourceInput() {
        // Create required inputs for offered resource
        final var keywordsList = Arrays.asList(offeredResourceKeywords.split(","));
        final var publisher = URI.create(offeredResourcePublisherUri);
        final var language = "en";
        final var license = URI.create(offeredResourceLicenseUri);
        final var owner = URI.create(offeredResourceOwnerUri);
        final var dataAccessEndpointDocs = URI.create(offeredResourceEndpointsDocsUri);

        // Create Offered Resource Input
        var offeredResourceInput = new OfferedResourceInput(
                keywordsList, publisher, language, license,
                owner, dataAccessEndpointDocs, PaymentMethod.FREE, new ArrayList<>());
        offeredResourceInput.setTitle(offeredResourceTitle);
        offeredResourceInput.setDescription(offeredResourceDescription);
        return offeredResourceInput;

    }

    /**
     * Routes that returns an example passenger info.
     *
     * @return an example passenger info.
     */
    @GetMapping("passenger-details")
    public ResponseEntity<String> getPassengerInfo() {
        return new ResponseEntity<>(
                "{"
                + "    \"flightNumber\": \"FL0001\","
                + "    \"baggageWeight\": 22.41,"
                + "    \"baggageSize\": [50, 60, 30]"
                + "}", HttpStatus.OK);
    }
}
