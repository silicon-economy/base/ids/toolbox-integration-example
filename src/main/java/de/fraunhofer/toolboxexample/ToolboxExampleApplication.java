/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

package de.fraunhofer.toolboxexample;

import javax.annotation.PostConstruct;

import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.idsintegrationtoolbox.utils.HealthChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


/**
 * Main class for the Toolbox Example application.
 *
 * @author Haydar Qarawlus
 * */
@SpringBootApplication(scanBasePackages = {"org.siliconeconomy.idsintegrationtoolbox",
    "de.fraunhofer.toolboxexample"})
@EntityScan("org.siliconeconomy.wrappertoolbox")
@EnableJpaRepositories(basePackages = {
    "org.siliconeconomy.idsintegrationtoolbox"
})
@Log4j2
public class ToolboxExampleApplication {
    
    @Autowired
    HealthChecker healthChecker;

    public static void main(String[] args) {
        SpringApplication.run(ToolboxExampleApplication.class, args);
    }
    
    @PostConstruct
    public void checkHealth() {
        log.info("Connector is reachable (health check): "
                + healthChecker.isConnectorReachable());
    }
}
