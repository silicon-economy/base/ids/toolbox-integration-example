/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

package de.fraunhofer.toolboxexample;


import java.net.URI;
import java.util.Collections;

import de.fraunhofer.iais.eis.Connector;
import de.fraunhofer.iais.eis.ResourceCatalog;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.idsintegrationtoolbox.DscOperator;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.AgreementArtifactRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.UuidUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * The Consumer offers and endpoint to send an example request to the DSC.
 *
 * @author Haydar Qarawlus
 */
@Log4j2
@RestController
@RequiredArgsConstructor
public class Consumer {

    /** The messages api operator. */
    private final @NonNull DscOperator dsc;

    /** The connector url of the DSC. */
    @Value("${connector.url:http://localhost:8080}")
    private String connectorBaseUri;
    
    /** The connector data api path. */
    @Value("${connector.api.path.data:/api/ids/data}")
    private String connectorDataPath;


    /**
     * Sends an example request to the DSC to fetch the data.
     *
     * @return the requested data.
     */
    @GetMapping("send-request")
    public ResponseEntity<String> sendRequest() {

        try {
            final var connectorUri = URI.create(this.connectorBaseUri + connectorDataPath);

            // Fetch connector description
            final var connectorDesc =
                    (Connector) dsc.ids().messages().descriptionRequest(connectorUri);

            // Fetch catalog
            final var catalogUri = connectorDesc.getResourceCatalog().get(0).getId();

            final var catalog =
                    (ResourceCatalog) dsc.ids().messages().descriptionRequest(connectorUri,
                            catalogUri);

            // Negotiate contract
            final var resource = catalog.getOfferedResource().get(0);
            final var resourceUri = resource.getId();
            final var contractUri = resource.getContractOffer().get(0).getId();
            final var artifactUri =
                    resource.getRepresentation().get(0).getInstance().get(0).getId();

            final var agreement = dsc.workflows().negotiation().negotiateContract(connectorUri,
                    Collections.singletonList(resourceUri),
                    Collections.singletonList(artifactUri),
                    Collections.singletonList(contractUri), true);

            // Get data by fetching the artifact agreement relation
            final var agreementUri = URI.create(agreement.getLinks().getSelf().getHref());
            final var agreementUuid = UuidUtils.getUuidFromUri(agreementUri);
            final var artifactAgreementRelationOutput =
                    (AgreementArtifactRelationOutput) dsc.entities().agreements().artifacts()
                            .getAll(agreementUuid);
            final var artifactLink =
                    artifactAgreementRelationOutput
                            .getEmbedded().getEntries().get(0).getLinks().getSelf().getHref();
            final var artifactUuid = UuidUtils.getUuidFromUri(URI.create(artifactLink));

            final var data = dsc.entities().artifacts().data().get(artifactUuid, false, null,
                    null, null, null, null);

            return new ResponseEntity<>(data.toString(), HttpStatus.OK);
        } catch (Exception e) {
            log.error("Exception caught: {}", e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
