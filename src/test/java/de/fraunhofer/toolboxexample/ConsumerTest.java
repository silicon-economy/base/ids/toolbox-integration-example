/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package de.fraunhofer.toolboxexample;

import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.validation.constraints.NotNull;

import de.fraunhofer.iais.eis.Action;
import de.fraunhofer.iais.eis.BaseConnectorBuilder;
import de.fraunhofer.iais.eis.Connector;
import de.fraunhofer.iais.eis.ConnectorEndpointBuilder;
import de.fraunhofer.iais.eis.ContractOfferBuilder;
import de.fraunhofer.iais.eis.PermissionBuilder;
import de.fraunhofer.iais.eis.RepresentationBuilder;
import de.fraunhofer.iais.eis.RepresentationInstance;
import de.fraunhofer.iais.eis.Resource;
import de.fraunhofer.iais.eis.ResourceBuilder;
import de.fraunhofer.iais.eis.ResourceCatalog;
import de.fraunhofer.iais.eis.ResourceCatalogBuilder;
import de.fraunhofer.iais.eis.SecurityProfile;
import de.fraunhofer.iais.eis.util.TypedLiteral;
import de.fraunhofer.iais.eis.util.Util;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.siliconeconomy.idsintegrationtoolbox.DscOperator;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.AgreementApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.AgreementArtifactsApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.ArtifactApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.DataApi;
import org.siliconeconomy.idsintegrationtoolbox.api.entity.EntityApi;
import org.siliconeconomy.idsintegrationtoolbox.api.ids.IdsApi;
import org.siliconeconomy.idsintegrationtoolbox.api.ids.MessagesApi;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.ContractNegotiationWorkflow;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.WorkflowApi;
import org.siliconeconomy.idsintegrationtoolbox.model.output.ResourceData;
import org.siliconeconomy.idsintegrationtoolbox.model.output.embedded.ArtifactEmbedded;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.AgreementLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.ArtifactLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.Link;
import org.siliconeconomy.idsintegrationtoolbox.model.output.relation.AgreementArtifactRelationOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.AgreementOutput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.ArtifactOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

@SpringBootTest(classes = {Consumer.class})
@ExtendWith(SpringExtension.class)
class ConsumerTest {

    @MockBean
    DscOperator dscOperator;
    
    @MockBean
    IdsApi idsApi;
    
    @MockBean
    MessagesApi messagesApi;

    @MockBean
    WorkflowApi workflowApi;

    @MockBean
    ContractNegotiationWorkflow contractNegotiationWorkflow;
    
    
    @MockBean
    EntityApi entityApi;
    
    @MockBean
    AgreementApi agreementApi;
    
    @MockBean
    ArtifactApi artifactApi;
    
    @MockBean
    DataApi dataApi;
    
    @MockBean
    AgreementArtifactsApi agreementArtifactsApi;

    @Mock
    AgreementOutput agreementOutput;

    @Mock
    AgreementLinks agreementLinks;

    @Mock
    Link link;

    @Mock
    AgreementArtifactRelationOutput agreementArtifactRelationOutput;

    @Mock
    ArtifactEmbedded artifactEmbedded;

    @Mock
    ArtifactOutput artifactOutput;

    @Mock
    ArtifactLinks artifactLinks;

    @Autowired
    Consumer consumer;
    
    @BeforeEach
    void setup() {
        Mockito.when(dscOperator.ids()).thenReturn(idsApi);
        Mockito.when(idsApi.messages()).thenReturn(messagesApi);
        Mockito.when(dscOperator.workflows()).thenReturn(workflowApi);
        Mockito.when(workflowApi.negotiation()).thenReturn(contractNegotiationWorkflow);
        Mockito.when(dscOperator.entities()).thenReturn(entityApi);
        Mockito.when(entityApi.agreements()).thenReturn(agreementApi);
        Mockito.when(agreementApi.artifacts()).thenReturn(agreementArtifactsApi);
        Mockito.when(entityApi.artifacts()).thenReturn(artifactApi);
        Mockito.when(artifactApi.data()).thenReturn(dataApi);
    }

    @Captor
    private ArgumentCaptor<URI> connectorUriArgumentCaptor;

    @Captor
    private ArgumentCaptor<URI> catalogUriArgumentCaptor;

    @Captor
    private ArgumentCaptor<List<URI>> resourceUriArgumentCaptor;

    @Captor
    private ArgumentCaptor<List<URI>> artifactUriArgumentCaptor;

    @Captor
    private ArgumentCaptor<List<URI>> contractUriArgumentCaptor;

    @Captor
    private ArgumentCaptor<UUID> agreementUuidArgumentCaptor;

    @Captor
    private ArgumentCaptor<UUID> artifactUuidArgumentCaptor;

    @Value("${connector.url:http://localhost:8080}")
    private String connectorUri;
    @Value("${connector.api.path.data:/api/ids/data}")
    private String connectorDataPath;

    @Test
    @SneakyThrows
    void sendRequest_validInputs_returnResponse() {
        // Arrange
        MockitoAnnotations.openMocks(this);
        final var expectedBody = new ResourceData("text/plain", "expectedBody".getBytes());
        final var connectorDesc = getConnectorDescription();
        final var resourceCatalog = getResourceCatalog();
        final var expectedUuid = "6f457874-6c05-4525-928e-717309a45597";
        final var expectedUri = "http://localhost:8080/api/agreements/6f457874-6c05-4525-928e" +
                "-717309a45597";

        Mockito.when(messagesApi.descriptionRequest(
                connectorUriArgumentCaptor.capture())).thenReturn(connectorDesc);
        Mockito.when(messagesApi.descriptionRequest(any(URI.class),
                catalogUriArgumentCaptor.capture())).thenReturn(resourceCatalog);
        Mockito.when(contractNegotiationWorkflow.negotiateContract(any(URI.class),
                resourceUriArgumentCaptor.capture(), artifactUriArgumentCaptor.capture(),
                contractUriArgumentCaptor.capture(), any(Boolean.class)))
                .thenReturn(agreementOutput);

        Mockito.when(agreementOutput.getLinks()).thenReturn(agreementLinks);
        Mockito.when(agreementLinks.getSelf()).thenReturn(link);
        Mockito.when(link.getHref()).thenReturn(expectedUri);

        Mockito.when(agreementArtifactsApi.getAll(agreementUuidArgumentCaptor.capture()))
                .thenReturn(agreementArtifactRelationOutput);

        Mockito.when(agreementArtifactRelationOutput.getEmbedded()).thenReturn(artifactEmbedded);
        Mockito.when(artifactEmbedded.getEntries())
                .thenReturn(Collections.singletonList(artifactOutput));
        Mockito.when(artifactOutput.getLinks()).thenReturn(artifactLinks);
        Mockito.when(artifactLinks.getSelf()).thenReturn(link);

        Mockito.when(dataApi.get(artifactUuidArgumentCaptor.capture(),
                        any(Boolean.class), eq(null), eq(null), eq(null), eq(null), eq(null)))
                .thenReturn(expectedBody);

        // Act
        final var result = consumer.sendRequest();

        // Assert
        assertThat(result.getBody()).isEqualTo(expectedBody.toString());
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);

        assertThat(connectorUriArgumentCaptor.getValue())
                .hasToString(connectorUri + connectorDataPath);
        assertThat(catalogUriArgumentCaptor.getValue())
                .isEqualTo(connectorDesc.getResourceCatalog().get(0).getId());
        assertThat(resourceUriArgumentCaptor.getValue().get(0))
                .isEqualTo(resourceCatalog.getOfferedResource().get(0).getId());
        assertThat(artifactUriArgumentCaptor.getValue().get(0))
                .hasToString("representationId");
        assertThat(contractUriArgumentCaptor.getValue().get(0))
                .hasToString(resourceCatalog.getOfferedResource().get(0)
                        .getContractOffer().get(0)
                        .getId().toString());
        assertThat(agreementUuidArgumentCaptor.getValue()).hasToString(expectedUuid);
        assertThat(artifactUuidArgumentCaptor.getValue()).hasToString(expectedUuid);
    }

    @Test
    @SneakyThrows
    void sendRequest_messageApiOperatorException_return500() {
        // Arrange
        Mockito.when(messagesApi.descriptionRequest(
                        connectorUriArgumentCaptor.capture()))
                .thenThrow(new ApiInteractionUnsuccessfulException("Unable to create resource"));

        // Act
        final var result = consumer.sendRequest();

        // Assert
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Test
    @SneakyThrows
    void sendRequest_artifactDataApiOperatorException_return500() {
        // Arrange
        MockitoAnnotations.openMocks(this);
        final var connectorDesc = getConnectorDescription();
        final var resourceCatalog = getResourceCatalog();
        final var expectedUuid = "6f457874-6c05-4525-928e-717309a45597";
        final var expectedUri = "http://localhost:8080/api/agreements/6f457874-6c05-4525-928e" +
                "-717309a45597";

        Mockito.when(messagesApi.descriptionRequest(
                connectorUriArgumentCaptor.capture())).thenReturn(connectorDesc);
        Mockito.when(messagesApi.descriptionRequest(any(URI.class),
                catalogUriArgumentCaptor.capture())).thenReturn(resourceCatalog);
        Mockito.when(contractNegotiationWorkflow.negotiateContract(any(URI.class),
                resourceUriArgumentCaptor.capture(), artifactUriArgumentCaptor.capture(),
                contractUriArgumentCaptor.capture(), any(Boolean.class)))
                .thenReturn(agreementOutput);

        Mockito.when(agreementOutput.getLinks()).thenReturn(agreementLinks);
        Mockito.when(agreementLinks.getSelf()).thenReturn(link);
        Mockito.when(link.getHref()).thenReturn(expectedUri);

        Mockito.when(agreementArtifactsApi.getAll(agreementUuidArgumentCaptor.capture()))
                .thenReturn(agreementArtifactRelationOutput);

        Mockito.when(agreementArtifactRelationOutput.getEmbedded()).thenReturn(artifactEmbedded);
        Mockito.when(artifactEmbedded.getEntries())
                .thenReturn(Collections.singletonList(artifactOutput));
        Mockito.when(artifactOutput.getLinks()).thenReturn(artifactLinks);
        Mockito.when(artifactLinks.getSelf()).thenReturn(link);

        Mockito.when(dataApi.get(artifactUuidArgumentCaptor.capture(),
                        any(Boolean.class), eq(null), eq(null), eq(null), eq(null), eq(null)))
                .thenThrow(new ApiInteractionUnsuccessfulException("Unable to create resource"));

        // Act
        final var result = consumer.sendRequest();

        // Assert
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);

        assertThat(connectorUriArgumentCaptor.getValue())
                .hasToString(connectorUri + connectorDataPath);
        assertThat(catalogUriArgumentCaptor.getValue())
                .isEqualTo(connectorDesc.getResourceCatalog().get(0).getId());
        assertThat(resourceUriArgumentCaptor.getValue().get(0))
                .isEqualTo(resourceCatalog.getOfferedResource().get(0).getId());
        assertThat(artifactUriArgumentCaptor.getValue().get(0))
                .hasToString("representationId");
        assertThat(contractUriArgumentCaptor.getValue().get(0))
                .hasToString(resourceCatalog.getOfferedResource().get(0)
                        .getContractOffer().get(0)
                        .getId().toString());

        assertThat(agreementUuidArgumentCaptor.getValue()).hasToString(expectedUuid);
        assertThat(artifactUuidArgumentCaptor.getValue()).hasToString(expectedUuid);
    }

    Connector getConnectorDescription() {
        return new BaseConnectorBuilder()
                ._securityProfile_(SecurityProfile.BASE_SECURITY_PROFILE)
                ._maintainer_(URI.create("https://some-url.com"))
                ._curator_(URI.create("https://some-url.com"))
                ._hasDefaultEndpoint_(new ConnectorEndpointBuilder()
                        ._accessURL_(URI.create("https://some-url.com"))
                        .build())
                ._inboundModelVersion_(Util.asList("4.0.0"))
                ._outboundModelVersion_("4.0.0")
                ._resourceCatalog_(Collections.singletonList(getResourceCatalog()))
                .build();
    }

    ResourceCatalog getResourceCatalog() {
        return new ResourceCatalogBuilder(URI.create("http://example" +
                ".org/resourceCatalog"))._offeredResource_(Collections.singletonList(getResource())).build();
    }

    Resource getResource() {
        final var representation = new RepresentationBuilder()
                ._instance_(Collections.singletonList(new RepresentationInstanceImpl()))
                .build();
        final var rule = new PermissionBuilder()
                ._action_(Collections.singletonList(Action.USE))
                .build();
        final var contract = new ContractOfferBuilder()
                ._permission_(Collections.singletonList(rule))
                .build();
        return new ResourceBuilder(URI.create("http://example.org/resource"))
                ._representation_(Collections.singletonList(representation))
                ._contractOffer_(Collections.singletonList(contract))
                .build();
    }

    static class RepresentationInstanceImpl implements RepresentationInstance {

        @Override
        public @NotNull URI getId() {
            return URI.create("representationId");
        }

        @Override
        public List<TypedLiteral> getLabel() {
            return null;
        }

        @Override
        public List<TypedLiteral> getComment() {
            return null;
        }

        @Override
        public String toRdf() {
            return null;
        }

        @Override
        public Map<String, Object> getProperties() {
            return null;
        }

        @Override
        public void setProperty(String s, Object o) {

        }
        @Override
        public RepresentationInstance deepCopy() {
            return null;
        }
    }
}
