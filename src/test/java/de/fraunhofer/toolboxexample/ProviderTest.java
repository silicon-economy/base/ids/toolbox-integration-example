/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package de.fraunhofer.toolboxexample;

import java.net.URI;
import java.net.URL;
import java.time.ZonedDateTime;

import de.fraunhofer.iais.eis.ids.jsonld.Serializer;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.siliconeconomy.idsintegrationtoolbox.DscOperator;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.ResourceCreationWorkflow;
import org.siliconeconomy.idsintegrationtoolbox.api.workflow.WorkflowApi;
import org.siliconeconomy.idsintegrationtoolbox.model.input.ArtifactInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.CatalogInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.ContractInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.OfferedResourceInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.RepresentationInput;
import org.siliconeconomy.idsintegrationtoolbox.model.input.RuleInput;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.Link;
import org.siliconeconomy.idsintegrationtoolbox.model.output.links.OfferedResourceLinks;
import org.siliconeconomy.idsintegrationtoolbox.model.output.single.OfferedResourceOutput;
import org.siliconeconomy.idsintegrationtoolbox.utils.exceptions.ApiInteractionUnsuccessfulException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {Provider.class})
class ProviderTest {

    /** The catalog title. */
    @Value("${catalog.title:Flight 0001 Baggage}")
    private String catalogTitle;

    /** The catalog description. */
    @Value("${catalog.description:The resource catalog containing baggage information}")
    private String catalogDescription;

    /** The data representation title. */
    @Value("${data-representation.title:DataRepresentationPas1}")
    private String dataRepresentationTitle;

    /** The access url. */
    @Value("${access-url:http://localhost:8081/passenger-details}")
    private String accessUrl;

    /** The artifact title. */
    @Value("${artifact.title:Passenger 1 Data}")
    private String artifactTitle;

    /** The contract title. */
    @Value("${contract.title:contract title}")
    private String contractTitle;

    /** The contract start time as ZonedDateTime string. */
    @Value("${contract.start-time:2021-08-06T13:33:44.995+02:00}")
    private String contractStartTime;

    /** The contract end time as ZonedDateTime string. */
    @Value("${contract.end-time:2021-09-06T13:33:44.995+02:00}")
    private String contractEndTime;

    /** The rule title. */
    @Value("${rule.title:Some Rule}")
    private String ruleTitle;

    /** The offered resource data access endpoint documentation uri. */
    @Value("${offered-resource.endpoints-docs-uri:https://example"
            + ".org/passengerDataAccessDocumentation}")
    private String offeredResourceEndpointsDocsUri;

    /** The offered resource title. */
    @Value("${offered-resource.title:PassengerBaggage1}")
    private String offeredResourceTitle;

    /** The offered resource description. */
    @Value("${offered-resource.description:Resource holding information about passenger baggage "
            + "of passenger 1}")
    private String offeredResourceDescription;

    @MockBean
    Serializer serializer;

    @MockBean
    DscOperator dscOperator;

    @MockBean
    WorkflowApi workflowApi;

    @Autowired
    Provider provider;

    @MockBean
    ResourceCreationWorkflow resourceCreation;

    @Captor
    private ArgumentCaptor<OfferedResourceInput> offeredResourceInputArgumentCaptor;

    @Captor
    private ArgumentCaptor<CatalogInput> catalogInputArgumentCaptor;

    @Captor
    private ArgumentCaptor<RepresentationInput> representationInputArgumentCaptor;

    @Captor
    private ArgumentCaptor<ArtifactInput> artifactInputArgumentCaptor;

    @Captor
    private ArgumentCaptor<ContractInput> contractInputArgumentCaptor;

    @Captor
    private ArgumentCaptor<RuleInput> ruleInputArgumentCaptor;

    @Test
    @SneakyThrows
    void createResource_validInputs_returnResponse() {
        // Arrange
        Mockito.when(dscOperator.workflows()).thenReturn(workflowApi);
        Mockito.when(workflowApi.resourceCreation()).thenReturn(resourceCreation);
        Mockito.when(serializer.serialize(any())).thenReturn("Some Rule");
        Mockito.when(resourceCreation.createResource(any(OfferedResourceInput.class),
                any(CatalogInput.class), any(RepresentationInput.class), any(ArtifactInput.class)
                , any(ContractInput.class), any(RuleInput.class))).thenReturn(getOfferedResource());
        // Act & Assert
        final var response = provider.createResource();
        Mockito.verify(resourceCreation, Mockito.times(1)).createResource(offeredResourceInputArgumentCaptor.capture(),
                catalogInputArgumentCaptor.capture(),
                representationInputArgumentCaptor.capture(),
                artifactInputArgumentCaptor.capture(),
                contractInputArgumentCaptor.capture(),
                ruleInputArgumentCaptor.capture());
        OfferedResourceInput offeredResourceInput = offeredResourceInputArgumentCaptor.getValue();
        CatalogInput catalogInput = catalogInputArgumentCaptor.getValue();
        RepresentationInput representationInput = representationInputArgumentCaptor.getValue();
        ArtifactInput artifactInput = artifactInputArgumentCaptor.getValue();
        ContractInput contractInput = contractInputArgumentCaptor.getValue();
        RuleInput ruleInput = ruleInputArgumentCaptor.getValue();

        // Response Assertions
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        // OfferedResourceInput Assertions
        assertThat(offeredResourceInput.getTitle()).isEqualTo(offeredResourceTitle);
        assertThat(offeredResourceInput.getDescription()).isEqualTo(offeredResourceDescription);
        assertThat(offeredResourceInput.getEndpointDocumentation()).isEqualTo(URI.create(offeredResourceEndpointsDocsUri));
        // CatalogInput Assertions
        assertThat(catalogInput.getTitle()).isEqualTo(catalogTitle);
        assertThat(catalogInput.getDescription()).isEqualTo(catalogDescription);
        // Representation Assertions
        assertThat(representationInput.getTitle()).isEqualTo(dataRepresentationTitle);
        // Artifact Assertions
        assertThat(artifactInput.getTitle()).isEqualTo(artifactTitle);
        assertThat(artifactInput.getAccessUrl()).isEqualTo(new URL(accessUrl));
        // Contract Assertions
        assertThat(contractInput.getTitle()).isEqualTo(contractTitle);
        assertThat(contractInput.getStart()).isEqualTo(ZonedDateTime.parse(contractStartTime));
        assertThat(contractInput.getEnd()).isEqualTo(ZonedDateTime.parse(contractEndTime));

        // Contract Rule Assertions
        assertThat(ruleInput.getTitle()).isEqualTo(ruleTitle);
    }

    @Test
    @SneakyThrows
    void createResource_resourceCreatorException_return500() {
        // Arrange
        Mockito.when(dscOperator.workflows()).thenReturn(workflowApi);
        Mockito.when(workflowApi.resourceCreation()).thenReturn(resourceCreation);

        Mockito.when(resourceCreation.createResource(any(OfferedResourceInput.class),
                any(CatalogInput.class), any(RepresentationInput.class), any(ArtifactInput.class),
                any(ContractInput.class), any(RuleInput.class)))
                .thenThrow(new ApiInteractionUnsuccessfulException("Unable to create resource"));

        // Act
        final var response = provider.createResource();

        Mockito.verify(resourceCreation, Mockito.times(1)).createResource(any(OfferedResourceInput.class),
                any(CatalogInput.class),
                any(RepresentationInput.class),
                any(ArtifactInput.class),
                any(ContractInput.class),
                any(RuleInput.class));
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Test
    @SneakyThrows
    void getPassengerInfo_normalRequest_returnInfo() {
        // Arrange
        final var expectedMessage = "{"
                + "    \"flightNumber\": \"FL0001\","
                + "    \"baggageWeight\": 22.41,"
                + "    \"baggageSize\": [50, 60, 30]"
                + "}";
        // Act
        final var response = provider.getPassengerInfo();
        // Assert
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(expectedMessage);
    }



    private OfferedResourceOutput getOfferedResource() {
        var offeredResourcOutput = new OfferedResourceOutput();
        ReflectionTestUtils.setField(offeredResourcOutput,
                "links", getOfferedResourceLinks());
        ReflectionTestUtils.setField(offeredResourcOutput,
                "title", offeredResourceTitle);
        return offeredResourcOutput;
    }

    private Link getLink() {
        var link = new Link();
        ReflectionTestUtils.setField(link, "href", "https://example.org");
        return link;
    }

    private OfferedResourceLinks getOfferedResourceLinks() {
        var offeredResourceLinks = new OfferedResourceLinks();
        ReflectionTestUtils.setField(offeredResourceLinks, "self", getLink());
        return offeredResourceLinks;
    }




}
