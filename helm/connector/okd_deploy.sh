#!/bin/bash
#
# Copyright 2021 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.
#

# Use Namespace already provided or the default given here
NAMESPACE="${NAMESPACE:=ids-wrapper-example}"

# Switch to project if it exists or create a new one
oc project "$NAMESPACE"
# Upgrade or install
helm upgrade --namespace "$NAMESPACE" -i connector .
# Ensure image stream picks up the new docker image right away
oc import-image connector:6.5.3
