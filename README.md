> This project has been archived. It is no longer maintained or updated. If you have any questions about the project, please contact info@openlogisticsfoundation.org.


<div align="center">
  <h2 align="center">IDS Wrapper Toolbox Integration Example</h2>
  Demonstrates the integration of the <a href="https://git.openlogisticfoundation.
org/silicon-economy/base/ids/wrapper-toolbox">IDS Integration Toolbox</a>.
</div>

## About
The *IDS Integration Toolbox* is a Java framework that aims to help developers to configure and 
manage
the [Dataspace Connector (DSC)](https://github.com/International-Data-Spaces-Association/DataspaceConnector)
on code level. It provides a bunch of useful tools and methods to easily integrate new or
existing systems into the IDS ecosystem. Therefore, deeper knowledge in the IDS concepts or DSC
API enpoints isn't needed.

This project aims at providing an example for the usage of the IDS Integration Toolbox, as the 
abstraction from pure IDS
components is omitted. By having a look at this project, integration help and identification for IDS use cases can be 
derived.

## Getting Started
To start the project, just run ``docker-compose build`` and `docker-compose up` in the root directory
of the project. After startup, the endpoints exposed by the wrapper component can be used to interact with the connector
instance that is being booted. Take a look at the controller implementations of the wrapper to see what's going on!