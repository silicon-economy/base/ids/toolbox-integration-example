FROM maven:3.6-openjdk-17-slim as build
RUN mkdir /app
WORKDIR /app
COPY pom.xml .
RUN mvn -f pom.xml dependency:go-offline

COPY src/ /app/src/
RUN mvn clean package -DskipTests

FROM openjdk:17-jdk-slim
RUN mkdir /app && chmod ugo+rwx /app

COPY --from=build /app/target/toolboxexample-*.jar /app/toolboxexample.jar
EXPOSE 8080
WORKDIR /app
ENTRYPOINT ["java", "-jar", "/app/toolboxexample.jar"]